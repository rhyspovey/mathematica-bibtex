(* ::Package:: *)

(* ::Title:: *)
(*BibTeX*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["BibTeX`"];


TeXDiacritics::usage="TeXDiacritics[string] replaces TeX diacritic commands with utf8. Based on internal $diacritics list that needs to be built up.";


TeXToLowerCase::usage="TeXToLowerCase[string] sends text to lower case, excluding any encapsulated in brackets whilst removing the brackets.";


AuthorList::usage="AuthorList[string] formats an author list from the BibTeX author field string.";


BibDate::usage="BibDate[association] extracts the date as a DateObject from a BibTeX association.";


TeXArticleReference::usage="TeXArticleReference[association] generates a TeX reference from a BibTeX association."


TeXProceedingsReference::usage="TeXProceedingsReference[association] generates a TeX reference from a BibTeX association."


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Subsection:: *)
(*Import*)


(* ::Text:: *)
(*TODO: add error handling for EndOfFile during an entry.*)


BibEntryFromStream[stream_InputStream]:=Module[{read,type,key,bracelevel,row,body},

(* read until @*)
While[(read=Read[stream,Character])=!="@",
If[read===EndOfFile,Return@EndOfFile];
];


(* read type *)
read=Read[stream,Character];
type=StringJoin@Reap[
While[StringMatchQ[read,WordCharacter],
Sow[read];read=Read[stream,Character];
]
][[2,1]];

If[type=="Comment",Return@Nothing];

(* read citation key*)
read=Read[stream,Character];
key=StringJoin@Reap[
While[(read!=",")\[And](read!="}"),
Sow[read];read=Read[stream,Character];
]
][[2,1]];

(* read through body using {,} as delimiters *)
bracelevel=0;row=1;
read=Read[stream,Character];
body=StringJoin/@Reap[While[(read!="}")\[Or](bracelevel>0),
If[read=="{",bracelevel++];
If[read=="}",bracelevel--];
If[(read==",")\[And](bracelevel==0),row++,Sow[read,row]];
read=Read[stream,Character];
]][[2]];

(* output *)
Join[
<|"Type"->type,"Citationkey"->key|>,
Association@@(StringCases[StringTrim@#,(\[FormalX]:WordCharacter..)~~(WhitespaceCharacter..)~~"="~~(WhitespaceCharacter..)~~Longest["{"~~\[FormalY]__~~"}"]:>(\[FormalX]->\[FormalY])]&/@body)
]

];


ImportBIB[filename_String]:=Module[{stream,read,output},

Reap[
stream=OpenRead[filename];
While[(read=BibEntryFromStream[stream])=!=EndOfFile,Sow[read]];
Close[stream]
][[2,1]]

];



ImportExport`RegisterImport["BIB",{"Data"->ImportBIB@#}&,"DefaultElement"->"Data"];


(* ::Subsection:: *)
(*Formatting*)


(* ::Text:: *)
(*This needs significant expanding as more are encountered.*)


$diacritics={
"\\'{E}"->"\[CapitalEAcute]","\\'E"->"\[CapitalEAcute]",
"\\'{c}"->"\[CAcute]","\\'c"->"\[CAcute]",
"\\c{S}"->"\:015e",
"\\v{s}"->"\[SHacek]"
};


TeXDiacritics[expr_String]:=StringReplace[expr,$diacritics]


TeXToLowerCase[expr_String]:=Module[{pos1,pos2,pos3},
pos1=StringPosition[expr,Shortest["{"~~__~~"}"]];
pos2=pos1+Most@FoldList[Plus,0,(Subtract@@#-1)&/@pos1];
pos3=pos1[[All,1]]-Range[0,2(Length[pos1]-1),2];

Fold[
StringInsert[#1,#2[[1]],#2[[2]]]&,
ToLowerCase[Fold[StringDrop,expr,pos2]],
MapThread[List,{
Table[StringTrim[StringTake[expr,\[FormalP]],"{"|"}"],{\[FormalP],pos1}],
pos3
}]
]
];


AuthorList[expr_String,OptionsPattern[{"Format"->"Initials","Last"->"Oxford comma and"}]]:=Module[{form1,form2},
form1=StringReplace[StringTrim[StringReplace[TeXDiacritics[#],Shortest["{"~~\[FormalX]__~~"}"]:>\[FormalX]]],\[FormalX]__~~", "~~\[FormalY]__:>\[FormalY]<>" "<>\[FormalX]]&/@StringSplit[expr," and "];
form2=Switch[OptionValue["Format"],
	"Initials",
	StringJoin[Riffle[#," "]]&/@(MapAt[StringJoin[Riffle[(StringTake[#,1]<>".")&/@StringSplit[#,"-"],"-"]]&,StringSplit[#],;;-2]&/@form1)
];
Switch[OptionValue["Last"],
	"Oxford comma and",
	StringJoin@Insert[Riffle[form2,", "],"and ",-2]
]

];


BibDate[bibentry_Association]:=If[KeyExistsQ[bibentry,"year"],
If[KeyExistsQ[bibentry,"month"],
	FromDateString[Capitalize[bibentry[["month"]]]<>" "<>bibentry[["year"]],{"MonthNameShort","Year"}],
	FromDateString[bibentry[["year"]],{"Year"}]
],
Missing[]
]


TeXDOILink[bibentry_Association]:=If[KeyExistsQ[bibentry,"doi"],
	"\\href{https://doi.org/"<>bibentry[["doi"]]<>"}{\\texttt{"<>StringReplace[bibentry[["doi"]],"_"->"\_"]<>"}}",
	Missing["doi"]
]


TeXArticleReference[bibentry_Association,OptionsPattern[{"AddDOI"->False}]]:=Module[{book,doi},
book=Which[
	AllTrue[{"journal","volume","pages"},KeyExistsQ[bibentry,#]&],
	StringJoin[
		"\\textit{",bibentry[["journal"]],"}"," ",
		"\\textbf{",bibentry[["volume"]],"}"," ",
		bibentry[["pages"]]
	],
	AllTrue[{"archiveprefix","primaryclass","eprint"},KeyExistsQ[bibentry,#]&],
	StringJoin[
		"\\texttt{",
		bibentry[["archiveprefix"]],":",
		bibentry[["eprint"]]," ",
		"[",bibentry[["primaryclass"]],"]",
		"}"
	],
	True,""
];
doi=TeXDOILink[bibentry];
StringJoin[
	AuthorList[bibentry[["author"]]],",\n",
	"``",Capitalize@TeXToLowerCase[bibentry[["title"]]],",''\n",
	If[KeyExistsQ[bibentry,"url"],
		"\\href{"<>bibentry[["url"]]<>"}{"<>book<>"}",
		book
	],"\n",
	"(",If[KeyExistsQ[bibentry,"month"],Capitalize[bibentry[["month"]]]<>" ",""],bibentry[["year"]],")",".",
	If[OptionValue["AddDOI"]\[And]Not[MissingQ[doi]],"\\\\\nDOI: "<>doi,""]
]
];


TeXProceedingsReference[bibentry_Association,OptionsPattern[{"AddDOI"->False}]]:=Module[{book,doi},
book=StringJoin[
		"\\textit{",bibentry[["booktitle"]],"}",
		If[KeyExistsQ[bibentry,"pages"],", "<>bibentry[["pages"]],""]
];
doi=TeXDOILink[bibentry];
StringJoin[
	AuthorList[bibentry[["author"]]],",\n",
	"``",Capitalize@TeXToLowerCase[bibentry[["title"]]],",''\n",
	If[KeyExistsQ[bibentry,"url"],
		"\\href{"<>bibentry[["url"]]<>"}{"<>book<>"}",
		book
	],"\n",
	"(",If[KeyExistsQ[bibentry,"month"],Capitalize[bibentry[["month"]]]<>" ",""],bibentry[["year"]],")",".",
	If[OptionValue["AddDOI"]\[And]Not[MissingQ[doi]],"\\\\\nDOI: "<>doi,""]
]
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
